#!/usr/bin/python

import spead2
import spead2.send
import numpy as np
import logging
import argparse
import random
import string
import os
import sys

class udp_send(object):

    def __init__(self, this_file):

        self.this_file        = this_file            
        self.thread_pool      = spead2.ThreadPool()   
        self.stream_config    = spead2.send.StreamConfig()
        self.stream           = spead2.send.UdpStream(self.thread_pool, '127.0.0.1',
                                9021, self.stream_config, 2000)

        logging.basicConfig(filename='send.log',
                level=logging.DEBUG,
                format='%(levelname)s %(asctime)s %(message)s')

    def check_file_exists(self, cfile):
        outcome = True
        if not os.path.isfile(cfile):
            outcome = False
        return outcome

    def id_gen(self):
        idstring = ''.join([random.choice(string.ascii_letters + string.digits) for n in range(16)])
        return idstring

    def send(self):
        if self.check_file_exists(self.this_file):
            pass
        else:
            raise OSError("No such file as {}".format(self.this_file))

        # Create item group to hold all our items
        item_group = spead2.send.ItemGroup()

        # Get a unique identifier for the file we are sending
        self.this_id = self.id_gen()
        item_group.add_item(None, name='identifier', description='Unique tranfer ID',
                shape=(len(self.this_id),), dtype=np.byte, value = bytearray(self.this_id, 'UTF8'))

        # Add the file name as an item 
        filename = os.path.basename(self.this_file)
        item_group.add_item(None, name='filename', description="Filename",
                shape=(len(filename),), dtype=np.byte, value = bytearray(filename, 'UTF8'))

        # Add the filesize as an item
        nbytes = str(os.path.getsize(self.this_file))
        item_group.add_item(None, name='nbytes', description="File size (bytes)",
                shape=(len(nbytes),), dtype=np.byte, value = bytearray(nbytes, 'UTF8'))

        # Open file we're going to send
        with open(self.this_file) as f:
            lines = f.readlines()
        
        # Add the number of lines we're sending as an itemgroup
        length = str(len(lines))
        item_group.add_item(None, name='nlines', description="Number of lines",
                shape=(len(length),), dtype=np.byte, value = bytearray(length, 'UTF8'))

        # Each line in the file is an item
        # Add each item to the item group
        line_number=1
        for line in lines:
            item_group.add_item(None, "dataline {}".format(line_number), "line",
                shape=(len(line),), dtype=np.byte, value = bytearray(line, 'UTF8'))
            line_number = line_number + 1

        # For item group into a heap
        heap = item_group.get_heap()

        # Send heap
        self.stream.send_heap(heap)
        logging.info("Start of stream")
        logging.info("Sending stream with id={}, name={}".format(self.this_id, filename))
        logging.info("Sending stream with id={}, nbytes={}".format(self.this_id, nbytes))
        logging.info("Sending stream with id={}, nlines={}".format(self.this_id, length))
        logging.info("End of stream")

        # Send "I've finished" 
        #self.stream.send_heap(item_group.get_end()) 

        print("File {} sent".format(self.this_file))

def main():
    parser = argparse.ArgumentParser(description='Sends files over UDP')
    parser.add_argument('-f','--file', help='File to send', required=True)
    args = parser.parse_args()

    sender = udp_send(args.file)
    sender.send()

if __name__ == '__main__':
    main()

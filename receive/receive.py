#!/usr/bin/python

from __future__ import print_function
import spead2
import spead2.recv
import numpy as np
import argparse
import sys
import logging

class udp_receive(object):

    def __init__(self, debug=False):
        
        self.thread_pool      = spead2.ThreadPool()
        self.stream           = spead2.recv.Stream(self.thread_pool)
        self.pool             = spead2.MemoryPool(16384, 26214400, 12, 8)
        self.debug            = debug

        self.stream.set_memory_allocator(self.pool)
        self.stream.add_udp_reader(9021, max_size=10000, buffer_size=10000)

        if self.debug:
            logging.basicConfig(level=logging.DEBUG,
                format='%(levelname)s %(asctime)s %(message)s')
        else:
            logging.basicConfig(filename='receive.log',
                level=logging.DEBUG,
                format='%(levelname)s %(asctime)s %(message)s')

    def is_file(self, values): 
        metadata = {}
        for item in values:
            line = bytearray(item.value).decode("utf-8").strip()
            if "identifier" in item.name:
                metadata['identifier'] = line
            if "filename" in item.name:
                metadata['filename'] = line
            if "nbytes" in item.name:
                metadata['nbytes'] = line
            if "nlines" in item.name:
                metadata['nlines'] = line
        return metadata

    def do_log(self):

        ident = self.metadata['identifier']
        name = self.metadata['filename']
        nbytes = self.metadata['nbytes']
        nlines = self.metadata['nlines']

        logging.info("Received stream with id={}, name={}".format(ident, name))
        logging.info("Received stream with id={}, nbytes={}".format(ident, nbytes))
        logging.info("Received stream with id={}, nlines={}".format(ident, nlines))

    def receive(self):
        print("Listening on port 9012..")
        for heap in self.stream:

            # Create empty item group to receive from heap
            ig = spead2.ItemGroup()

            # Update item group with contents of new heap
            ig.update(heap)

            # Check for a filename + metadata
            self.metadata = self.is_file(ig.values())

            # Check we have a filename, ignore the stream (for now) if not
            try:
                self.metadata['filename']
            except KeyError:
                logging.warning("No filename for stream with id={}".format(self.metadata['identifier']))
                continue

            # Log stream metadata
            self.do_log()

            # Unpack file contents from item group
            with open(self.metadata['filename'], 'a') as outfile:

                for item in ig.values():
                    line = bytearray(item.value).decode("utf-8").strip()
                    if "dataline" in item.name:
                        outfile.write(line + "\n")
                        print(item.name, line)

def main():
    parser = argparse.ArgumentParser(description='Receives files over UDP')
    parser.add_argument('-d', '--debug', help='Enable debug mode', required=False, action='store_true')
    args = parser.parse_args()

    receiver = udp_receive(args.debug)
    receiver.receive()

if __name__ == '__main__':
    main()
